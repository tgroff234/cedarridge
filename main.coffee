$(document).ready () ->

  #Dynamically load content partials
  
  $("#summary").load("./summary.html")
  $('#amenities').load("./amenities.html")
  $('#attractions').load("./attractions.html")
  $('#pricing').load("./pricing.html")



  # Attach Navbar item click functionality
  
  $("#foust").click () ->
    $("#foust-internal").toggleClass("active")
    $("#harned-internal").removeClass("active")
  $("#harned").click () ->
    $("#harned-internal").toggleClass("active")
    $("#foust-internal").removeClass("active")

  $(".brand").click () ->
    $("html, body").animate {
    	scrollTop: 0
    }, 1000

  attach_nav = (elem) ->
  	$(elem).click () ->
  	  console.log(elem)
  	  $("html, body").animate {
  	  	scrollTop: $($(elem).data("target")).offset().top - 80
  	  }, 1000

  attach_nav(e) for e in $(".nav-item").toArray()

  
  # Navbar animation
  
  $(window).scroll () ->
    navbar = $(".navbar")
    if $(window).scrollTop() > 10
  	  navbar.addClass("floating")
    else
  	  navbar.removeClass("floating")


